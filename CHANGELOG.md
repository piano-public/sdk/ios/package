# Piano SDK for iOS

## v2.8.6
* Fixes privacy manifests

## v2.8.5
* [PianoOAuth] - Added extended error information

## v2.8.4
* [PianoComposer] - Added feature to change pageViewId
* [PianoTemplate] - Added feature to change parameters

## v2.8.3
* [PianoComposer] - Added a function to disable interceptors

## v2.8.2
* [PianoOAuth] - Fixed popup webview for Apple Sign In

## v2.8.1
* Experemental support for visionOS
* Fixes privacy manifests for CocoaPods

## v2.8.0
* Added privacy manifest

## v2.7.12
* [PianoComposer] - Fixed JSON serialization for customVariables

## v2.7.11
* [PianoComposer] - Fixed customVariables for templates

## v2.7.10
* [PianoComposer] - Added thread safe support for customVariables

## v2.7.9
* Fixed bugs

## v2.7.8
* Fixed bugs

## v2.7.7
* [PianoComposer] - Added Edge support
* [PianoComposer] - Fixed storage tac cookie
* [PianoTemplate] - Fixed tbc for ShowTemplate

## v2.7.6
* [PianoOAuth] - Fixed bugs

## v2.7.5
* Added AID to HTTP headers
* [PianoComposer] - Added bid
* [PianoComposer] - Updated pageViewId generation format

## v2.7.4
* [PianoOAuth] - Fixed bugs

## v2.7.3
* [PianoOAuth] - Changed endpoint definition

## v2.7.2
* [PianoOAuth] - Fixed bugs
* [PianoTemplate] - Updated recommendations url

## v2.7.1
* Added PianoConsents support

## v2.7.0
* [PianoOAuth] - Google Sign In moved to external repository
* [PianoOAuth] - Facebook Login moved to external repository
* [PianoC1X] - Moved to external repository

## v2.6.6
* [PianoOAuth] - Updated Google SDK and Facebook SDK

## v2.6.5
* [PianoOAuth] - Fixed signIn handler call
* [PianoTemplate] - Added extended parameters for modal templates

## v2.6.4
* [PianoOAuth] - Fixed signIn handler call for canceling authorization

## v2.6.3
* [PianoOAuth] - Added a cancel flag for the signIn handler

## v2.6.2
* [PianoOAuth] - Added function for error handling

## v2.6.1
* Fixes for iOS 13+

## v2.6.0
* Updated minimum requirements for iOS version to 12.0
* Updated minimum requirements for Swift version to 5.7
* [PianoOAuth] - Updated Facebook and Google sign-in components

## v2.5.5
* [PianoOAuth] - Added function for update user info
* [PianoComposer] - Added tracking functions

## v2.5.4
* [PianoTemplate] - Fixed memory leak

## v2.5.3
* [PianoComposer] - Added request parameters (title, description, etc...)

## v2.5.2
* [PianoID] - Added getting user information
* [PianoTemplate] - Added custom form tracking

## v2.5.1
* [PianoComposer] - Added support accessToken for CDN
* [PianoComposer] - Added support forms
* [PianoComposer] - Added support recommendations
* [PianoTemplate] - Added support form template
* [PianoTemplate] - Added support recommendations template

## v2.5.0
* Added Swift Package Manager support
* [PianoCommon] Common functionality moved to a new component PianoCommon
* [PianoComposer] Added Apple TV support

## v2.4.2
* [PianoComposer] Fixed activity indicator
* [PianoOAuth] Fixed cancel handler

## v2.4.1
* [PianoComposer] Fixed calling composerExecutionCompleted method for PianoComposerDelegate
* [PianoComposer] Added support "Set response variable"
* [PianoC1X] Cxense SDK updated to v1.9.7

## v2.4.0
* [Piano Composer] Added support C1X
* [Piano ID] Removed deprecated functions

## v2.3.13
* [Piano ID] Added email confirmation flag
* [Piano ID] Fix problems with saved cookies

## v2.3.12

* Changed endpoint structure for PianoComposer
* Added static endpoints for PianoComposer (production, production-australia, production-asia-pacific, sandbox)
* Changed handlers in `PianoIDDelegate`
* Added `customEvent` handler to `PianoIDDelegate`
* Added `incremented` parameter for `PageViewMeterEventParams`
