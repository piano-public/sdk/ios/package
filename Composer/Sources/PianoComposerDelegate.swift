import Foundation

#if canImport(ObjectiveC)
@objc
#endif
public protocol PianoComposerDelegate: AnyObject {
    /**
     Show login event
     */
    #if canImport(ObjectiveC)
    @objc
    #endif
    optional func showLogin(composer: PianoComposer, event: XpEvent, params: ShowLoginEventParams?)
    
    /**
     Show template event
     */
    #if canImport(ObjectiveC)
    @objc
    #endif
    optional func showTemplate(composer: PianoComposer, event: XpEvent, params: ShowTemplateEventParams?)
    
    /**
     Show form event
     */
    #if canImport(ObjectiveC)
    @objc
    #endif
    optional func showForm(composer: PianoComposer, event: XpEvent, params: ShowFormEventParams?)
    
    /**
     Show recommendations event
     */
    #if canImport(ObjectiveC)
    @objc
    #endif
    optional func showRecommendations(composer: PianoComposer, event: XpEvent, params: ShowRecommendationsEventParams?)
    
    /**
     Set response variable event
     */
    #if canImport(ObjectiveC)
    @objc
    #endif
    optional func setResponseVariable(composer: PianoComposer, event: XpEvent, params: SetResponseVariableParams?)
    
    /**
     Non site action event
     */
    #if canImport(ObjectiveC)
    @objc
    #endif
    optional func nonSite(composer: PianoComposer, event: XpEvent)
    
    /**
     User segment true event
     */
    #if canImport(ObjectiveC)
    @objc
    #endif
    optional func userSegmentTrue(composer: PianoComposer, event: XpEvent)
    
    /**
     User segment false event
     */
    #if canImport(ObjectiveC)
    @objc
    #endif
    optional func userSegmentFalse(composer: PianoComposer, event: XpEvent)
    
    /**
     Meter active event
     */
    #if canImport(ObjectiveC)
    @objc
    #endif
    optional func meterActive(composer: PianoComposer, event: XpEvent, params: PageViewMeterEventParams?)
    
    /**
     Meter expired event
     */
    #if canImport(ObjectiveC)
    @objc
    #endif
    optional func meterExpired(composer: PianoComposer, event: XpEvent, params: PageViewMeterEventParams?)
    
    /**
     Exeperience execution failed
     */
    #if canImport(ObjectiveC)
    @objc
    #endif
    optional func experienceExecutionFailed(composer: PianoComposer, event: XpEvent, params: FailureEventParams?)
    
    /**
     Exeperience execute event
     */
    #if canImport(ObjectiveC)
    @objc
    #endif
    optional func experienceExecute(composer: PianoComposer, event: XpEvent, params: ExperienceExecuteEventParams?)
    
    /**
     Event fired by composer when async task was completed and all experience event fired
     */
    #if canImport(ObjectiveC)
    @objc
    #endif
    optional func composerExecutionCompleted(composer: PianoComposer)
}
