import Foundation

#if canImport(UIKit)
import UIKit
fileprivate let MODEL = UIDevice.current.model
#else
fileprivate let MODEL = "Unknown"
#endif

import PianoCommon
import PianoConsents

public class ComposerHelper {

    private static let pageViewIdRegex = try! NSRegularExpression(pattern: "[a-z0-9]{1,16}", options: .caseInsensitive)
    
    public static func generateUserAgent() -> String {
        return "Piano composer SDK (iOS \(ProcessInfo.processInfo.operatingSystemVersionString) like Mac OS X; \(MODEL))"
    }
    
    public static func generatePageViewId() -> String {
        var pageViewId = String(Date().toUnixTimestamp(), radix: 36)
        while pageViewId.count < 16 {
            pageViewId += String(Int.random(in: 0...Int.max), radix: 36)
        }
        
        return String(pageViewId.prefix(16))
    }
    
    internal static func checkPageViewId(_ pageViewId: String) throws {
        if pageViewIdRegex.numberOfMatches(in: pageViewId, range: NSRange(location: 0, length: pageViewId.count)) != 1 {
            throw PianoError("Invalid pageViewId \(pageViewId)")
        }
    }
    
    internal static func generateVisitId() -> String {
        return "v-" + generatePageViewId();
    }
    
    // time zone offset in minutes
    internal static func getTimeZoneOffset() -> Int {
        return TimeZone.current.secondsFromGMT() / 60
    }
    
    internal static func getSdkVersion() -> String {
        if let version = Bundle(for: PianoComposer.self).infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }

        return "n/a";
    }
}
