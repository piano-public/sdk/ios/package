import Foundation

@objcMembers
public class TemplateEventParams: NSObject {
    
    public let templateId: String
    public let templateVariantId: String
    
    public var displayMode: DisplayMode
    public var containerSelector: String
    public var showCloseButton: Bool
    
    internal(set) public var endpointUrl: String = ""
    internal(set) public var trackingId: String = ""
    
    internal init(d: [String: Any]) {
        templateId = d["templateId"] as? String ?? ""
        templateVariantId = d["templateVariantId"] as? String ?? ""
        displayMode = DisplayMode(name: (d["displayMode"] as? String ?? ""))
        containerSelector = d["containerSelector"] as? String ?? ""
        showCloseButton = d["showCloseButton"] as? Bool ?? false
    }
}
