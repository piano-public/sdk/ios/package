import Foundation

@objcMembers
public class UserSegments: NSObject {
 
    public let standart: UserSegmentsInfo?
    
    public let composer1x: UserSegmentsInfo?
    
    init(dict: [String: Any]) {
        if let d = dict["STANDARD"] as? [String:Any] {
            standart = UserSegmentsInfo(dict: d)
        } else {
            standart = nil
        }
        
        if let d = dict["COMPOSER1X"] as? [String:Any] {
            composer1x = UserSegmentsInfo(dict: d)
        } else {
            composer1x = nil
        }
    }
    
    func toMap() -> [String:Any] {
        var map: [String:Any] = [:]
        if let standart {
            map["STANDARD"] = [
                "segments": standart.segments,
                "expiresAt": standart.expiresAt.timeIntervalSince1970
            ]
        }
        
        if let composer1x {
            map["COMPOSER1X"] = [
                "segments": composer1x.segments,
                "expiresAt": composer1x.expiresAt.timeIntervalSince1970
            ]
        }
        
        return map
    }
}
