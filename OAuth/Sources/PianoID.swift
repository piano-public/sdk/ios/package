import AuthenticationServices
import CommonCrypto
import UIKit
import SafariServices

import PianoCommon
import PianoConsents

public typealias PianoIDSignInHandler = (PianoIDSignInResult?, Error?, Bool) -> Void

@objcMembers
public class PianoID: NSObject {

    public static let shared: PianoID = PianoID()

    public weak var delegate: PianoIDDelegate?
    internal weak var authViewController: PianoIDOAuthViewController?

    private let deploymentHostPath = "/api/v3/anon/mobile/sdk/id/deployment/host"
    private let authorizationPath = "/id/api/v1/identity/vxauth/authorize"
    private let passwordlessPath = "/id/api/v1/identity/passwordless/authorization/code"
    private let tokenPath = "/id/api/v1/identity/oauth/token"
    private let logoutPath = "/id/api/v1/identity/logout"
    private let formInfoPath = "/id/api/v1/identity/userinfo"

    private var urlSession: URLSession

    public var aid: String = ""
    public var endpoint: PianoEndpoint = PianoEndpoint.production
    public var deploymentHost: String = ""
    public var signUpEnabled = false
    public var widgetType: WidgetType = .login
    public var stage = ""
    public var presentingViewController: UIViewController?
    public var extendedErrors = false

    @available(*, deprecated, message: "Set GIDClientID property https://developers.google.com/identity/sign-in/ios/start-integrating#add_client_id")
    public var googleClientId: String = ""
    
    internal var nativeSignInWithAppleEnabled = false
    internal var socialProviders: [String:PianoIDSocialProvider] = [:]

    private let urlSchemePrefix = "io.piano.id"
    private let urlSchemePath = "success"

    private var redirectScheme: String {
        get {
            "\(urlSchemePrefix).\(aid.lowercased())://"
        }
    }

    private var redirectURI: String {
        get {
            "\(redirectScheme)\(urlSchemePath)"
        }
    }

    fileprivate var _currentToken: PianoIDToken?
    public var currentToken: PianoIDToken? {
        if _currentToken == nil {
            _currentToken = PianoIDTokenStorage.shared.loadToken(aid: getAID())
        }
        return _currentToken
    }

    private override init() {
        let config = URLSessionConfiguration.default
        config.httpCookieStorage = nil
        config.httpCookieAcceptPolicy = .never
        urlSession = URLSession(configuration: config)
        super.init()
    }

    public func getAID() -> String {
        assert(!aid.isEmpty, "PIANO_ID: Piano AID should be specified")
        return aid
    }

    public func signIn(completion: PianoIDSignInHandler? = nil) {
        getDeploymentHost { (host) in
            if let url = self.prepareAuthorizationUrl(host: host) {
                self.startAuthSession(url: url, completion: completion)
            } else {
                self.signInFail(PianoIDError.invalidAuthorizationUrl, completion: completion)
            }
        }
    }
    
    public func add(socialProvider: PianoIDSocialProvider) {
        let name = socialProvider.name.lowercased()
        if name.isEmpty || socialProviders.contains(where: { $0.key == name }) {
            return
        }
        
        socialProviders[name] = socialProvider
    }

    fileprivate func passwordlessSignIn(code: String) {
        getDeploymentHost { (host) in
            if let url = self.preparePasswrodlessUrl(host: host, code: code) {
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                PianoID.addHeaders(request: &request)
                let dataTask = self.urlSession.dataTask(with: request) { (data, response, error) in
                    if error == nil, let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let responseData = data {
                        if let token = self.parseToken(response: response!, responseData: responseData) {
                            self.signInSuccess(token, false)
                            return
                        }
                    }
                }

                dataTask.resume()
            }
        }
    }

    public func signOut(token: String, completion: ((Error?) -> Void)? = nil) {
        _currentToken = nil
        PianoIDTokenStorage.shared.removeToken(aid: getAID())

        getDeploymentHost { (host) in
            if let url = self.prepareSignOutUrl(host: host, token: token) {
                var request = URLRequest(url: url)
                request.httpMethod = "GET"
                PianoID.addHeaders(request: &request)
                let dataTask = self.urlSession.dataTask(with: request) { (data, response, error) in
                    if let error {
                        completion?(error)
                        self.signOutFail()
                        return
                    }
                    
                    if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 {
                        completion?(nil)
                        self.signOutSuccess()
                    } else {
                        completion?(PianoIDError.signOutFailed)
                        self.signOutFail()
                    }
                }

                dataTask.resume()
            }
        }
    }

    public func refreshToken(_ refreshToken: String, completion: @escaping (PianoIDToken?, PianoIDError?) -> Void) {
        getDeploymentHost { (host) in
            if let url = self.prepareRefreshTokenUrl(host: host) {
                let body: [String: Any] = [
                    "client_id": self.getAID(),
                    "grant_type": "refresh_token",
                    "refresh_token": refreshToken
                ]

                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                PianoID.addHeaders(request: &request)
                request.httpBody = JSONSerializationUtil.serializeObjectToJSONData(object: body)
                let dataTask = self.urlSession.dataTask(with: request) { (data, response, error) in
                    if error == nil, let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let responseData = data {
                        if let token = self.parseToken(response: response!, responseData: responseData) {
                            self._currentToken = token
                            _ = PianoIDTokenStorage.shared.saveToken(token, aid: self.getAID())
                            
                            completion(token, nil)
                            return
                        }
                    }

                    completion(nil, PianoIDError.refreshFailed)
                }

                dataTask.resume()
            }
        }
    }
    
    public func formInfo(aid: String, accessToken: String, formName: String? = nil, completion: @escaping (PianoIDFormInfo?, Error?) -> Void) {
        getDeploymentHost { (host) in
            if let url = self.prepareFormInfoUrl(host: host, aid: aid, formName: formName) {
                var request = URLRequest(url: url)
                request.httpMethod = "GET"
                request.setValue(accessToken, forHTTPHeaderField: "Authorization")
                PianoID.addHeaders(request: &request)
                let dataTask = self.urlSession.dataTask(with: request) { (data, response, error) in
                    do {
                        let (httpResponse, d) = try self.check(error: error, response: response, data: data)
                        
                        guard let formInfo = self.parseFormInfo(response: httpResponse, responseData: d) else {
                            throw PianoError("Failed parse formInfo data")
                        }
                        
                        completion(formInfo, nil)
                    } catch {
                        completion(nil, self.extendedErrors ? error : PianoIDError.formInfoFailed)
                    }
                }

                dataTask.resume()
            }
        }
    }
    
    public func formInfo(accessToken: String, formName: String? = nil, completion: @escaping (PianoIDFormInfo?, Error?) -> Void) {
        formInfo(aid: getAID(), accessToken: accessToken, completion: completion)
    }
    
    public func userInfo(aid: String, accessToken: String, formName: String, completion: @escaping (PianoIDUserInfo?, Error?) -> Void) {
        getDeploymentHost { (host) in
            if let url = self.prepareFormInfoUrl(host: host, aid: aid, formName: formName) {
                var request = URLRequest(url: url)
                request.httpMethod = "GET"
                request.setValue(accessToken, forHTTPHeaderField: "Authorization")
                PianoID.addHeaders(request: &request)
                let dataTask = self.urlSession.dataTask(with: request) { (data, response, error) in
                    do {
                        let (httpResponse, d) = try self.check(error: error, response: response, data: data)
                        
                        guard let userInfo = self.parseUserInfo(response: httpResponse, responseData: d) else {
                            throw PianoError("Failed parse userInfo data")
                        }
                        
                        completion(userInfo, nil)
                    } catch {
                        completion(nil, self.extendedErrors ? error : PianoIDError.userInfoFailed)
                    }
                }

                dataTask.resume()
            }
        }
    }
    
    public func userInfo(accessToken: String, formName: String, completion: @escaping (PianoIDUserInfo?, Error?) -> Void) {
        userInfo(aid: getAID(), accessToken: accessToken, formName: formName, completion: completion)
    }
    
    public func putUserInfo(
        aid: String,
        accessToken: String,
        formName: String,
        customFields: [String:String],
        completion: @escaping (PianoIDUserInfo?, Error?) -> Void
    ) {
        getDeploymentHost { (host) in
            if let url = self.prepareUpdateUserInfoUrl(host: host, aid: aid) {
                var request = URLRequest(url: url)
                request.httpMethod = "PUT"
                request.setValue(accessToken, forHTTPHeaderField: "Authorization")
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                PianoID.addHeaders(request: &request)
                
                let fields: [[String:Any]] = customFields.map { k,v in
                    [
                        "field_name": k,
                        "value": v
                    ]
                }
                
                let body : [String:Any] = [
                    "form_name": formName,
                    "custom_field_values": fields
                ]
                
                request.httpBody = JSONSerializationUtil.serializeObjectToJSONData(object: body)
                
                let dataTask = self.urlSession.dataTask(with: request) { (data, response, error) in
                    do {
                        let (httpResponse, d) = try self.check(error: error, response: response, data: data)
                        
                        guard let userInfo = self.parseUserInfo(response: httpResponse, responseData: d) else {
                            throw PianoError("Failed parse userInfo data")
                        }
                        
                        completion(userInfo, nil)
                    } catch let error as PianoHTTPError {
                        completion(nil, self.extendedErrors ? error : (error.statusCode == 400 ? PianoIDError.userInfoValidationFailed : PianoIDError.userInfoFailed))
                    } catch {
                        completion(nil, self.extendedErrors ? error : PianoIDError.userInfoFailed)
                    }
                }

                dataTask.resume()
            }
        }
    }
    
    public func putUserInfo(
        accessToken: String,
        formName: String,
        customFields: [String:String],
        completion: @escaping (PianoIDUserInfo?, Error?) -> Void
    ) {
        putUserInfo(aid: getAID(), accessToken: accessToken, formName: formName, customFields: customFields, completion: completion)
    }

    fileprivate func parseToken(response: URLResponse, responseData: Data) -> PianoIDToken? {
        if let responseObject = JSONSerializationUtil.deserializeResponse(response: response, responseData: responseData) {
            if let accessToken = responseObject["access_token"] as? String,
               let refreshToken = responseObject["refresh_token"] as? String {
                return PianoIDToken(accessToken: accessToken, refreshToken: refreshToken)
            }
        }

        return .none
    }
    
    fileprivate func parseFormInfo(response: URLResponse, responseData: Data) -> PianoIDFormInfo? {
        if let responseObject = JSONSerializationUtil.deserializeResponse(response: response, responseData: responseData) {
            let hasAllCustomFieldValuesFilled = responseObject["has_all_custom_field_values_filled"] as? Bool ?? false
            return PianoIDFormInfo(hasAllCustomFieldValuesFilled: hasAllCustomFieldValuesFilled)
        }
        
        return .none
    }
    
    fileprivate func parseUserInfo(response: URLResponse, responseData: Data) -> PianoIDUserInfo? {
        if let responseObject = JSONSerializationUtil.deserializeResponse(response: response, responseData: responseData) {
            var customFields: [PianoIDUserInfoCustomField] = []
            if let cfs = responseObject["custom_field_values"] as? [[String:Any]] {
                cfs.forEach { cf in
                    customFields.append(
                        PianoIDUserInfoCustomField(
                            fieldName: cf["field_name"] as? String ?? "",
                            value: cf["value"] as? String ?? "",
                            created: Date(timeIntervalSince1970: cf["created"] as? Double ?? 0),
                            emailCreator: cf["email_creator"] as? String,
                            sortOrder: cf["sort_order"] as? Int64 ?? 0
                        )
                    )
                }
            }
            
            return PianoIDUserInfo(
                email: responseObject["email"] as? String ?? "",
                uid: responseObject["uid"] as? String ?? "",
                firstName: responseObject["first_name"] as? String ?? "",
                lastName: responseObject["last_name"] as? String ?? "",
                aid: responseObject["aid"] as? String ?? "",
                updated: Date(timeIntervalSince1970: responseObject["updated"] as? Double ?? 0),
                linkedSocialAccounts: responseObject["linked_social_accounts"] as? [String] ?? [],
                passwordAvailable: (responseObject["password_available"] as? Bool) ?? false,
                customFields: customFields,
                allCustomFieldValuesFilled: responseObject["has_all_custom_field_values_filled"] as? Bool ?? false,
                needResendConfirmationEmail: responseObject["need_resend_confirmation_email"] as? Bool ?? false,
                changedEmail: responseObject["changed_email"] as? Bool ?? false,
                passwordless: responseObject["passwordless"] as? Bool ?? false
            )
        }
        
        return .none
    }

    fileprivate func getDeploymentHost(completion: @escaping (String) -> Void) {
        guard deploymentHost.isEmpty else {
            completion(deploymentHost)
            return
        }
        
        deploymentHost = endpoint.id

        if let url = prepareDeploymentHostUrl(host: endpoint.api) {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue(aid, forHTTPHeaderField: "piano-app-id")

            let dataTask = urlSession.dataTask(with: request) { (data, response, error) in
                if error == nil, let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let responseData = data {
                    if let responseObject = JSONSerializationUtil.deserializeResponse(response: response!, responseData: responseData), let host = responseObject["data"] as? String {
                        if !host.isEmpty {
                            self.deploymentHost = host
                        }
                    }
                }

                completion(self.deploymentHost)
            }

            dataTask.resume()
        }
    }

    private func prepareDeploymentHostUrl(host: String) -> URL? {
        var urlComponents = URLComponents(string: host)
        urlComponents?.path = deploymentHostPath
        urlComponents?.queryItems = [
            URLQueryItem(name: "aid", value: getAID()),
        ]

        return urlComponents?.url
    }

    private func prepareAuthorizationUrl(host: String) -> URL? {
        guard var urlComponents = URLComponents(string: host) else {
            return nil
        }

        var queryItems: [URLQueryItem] = [
            URLQueryItem(name: "client_id", value: getAID()),
            URLQueryItem(name: "screen", value: widgetType.description),
            URLQueryItem(name: "disable_sign_up", value: "\(!signUpEnabled)"),
            URLQueryItem(name: "response_type", value: "token"),
            URLQueryItem(name: "is_sdk", value: "\(true)"),
            URLQueryItem(name: "redirect_uri", value: redirectURI),
        ]
        
        if !stage.isEmpty {
            queryItems.append(URLQueryItem(name: "stage", value: stage))
        }

        var nativeOAuthProviders: [String] = socialProviders.keys.map { $0 }
        if nativeSignInWithAppleEnabled {
            nativeOAuthProviders.append("apple")
        }

        if !nativeOAuthProviders.isEmpty {
            queryItems.append(URLQueryItem(name: "oauth_providers", value: nativeOAuthProviders.joined(separator: ",")))
        }

        urlComponents.path = authorizationPath
        urlComponents.queryItems = queryItems
        return urlComponents.url
    }

    private func preparePasswrodlessUrl(host: String, code: String) -> URL? {
        guard var urlComponents = URLComponents(string: host) else {
            return nil
        }

        let queryItems: [URLQueryItem] = [
            URLQueryItem(name: "aid", value: getAID()),
            URLQueryItem(name: "passwordless_token", value: code)
        ]

        urlComponents.queryItems = queryItems
        urlComponents.path = passwordlessPath
        return urlComponents.url
    }

    private func prepareRefreshTokenUrl(host: String) -> URL? {
        guard var urlComponents = URLComponents(string: host) else {
            return nil
        }

        urlComponents.path = tokenPath
        return urlComponents.url
    }

    private func prepareSignOutUrl(host: String, token: String) -> URL? {
        guard var urlComponents = URLComponents(string: host) else {
            return nil
        }

        urlComponents.path = logoutPath
        urlComponents.queryItems = [
            URLQueryItem(name: "client_id", value: getAID()),
            URLQueryItem(name: "token", value: token),
        ]

        return urlComponents.url
    }
    
    private func prepareFormInfoUrl(host: String, aid: String, formName: String?) -> URL? {
        guard var urlComponents = URLComponents(string: host) else {
            return nil
        }
        
        urlComponents.path = formInfoPath
        
        var queryItems = [
            URLQueryItem(name: "client_id", value: aid)
        ]
        
        if let f = formName {
            queryItems.append(URLQueryItem(name: "form_name", value: f))
        }
        
        urlComponents.queryItems = queryItems
        
        return urlComponents.url
    }
    
    private func prepareUpdateUserInfoUrl(host: String, aid: String) -> URL? {
        guard var urlComponents = URLComponents(string: host) else {
            return nil
        }
        
        urlComponents.path = formInfoPath
        
        urlComponents.queryItems = [
            URLQueryItem(name: "aid", value: aid)
        ]
        
        return urlComponents.url
    }

    private func startAuthSession(url: URL, completion: PianoIDSignInHandler? = nil) {
        if authViewController != nil {
            return
        }

        DispatchQueue.main.async {
            if let presentingViewController = self.getPresentingViewController() {
                let authViewController = PianoIDOAuthViewController(title: "", url: url, completion: completion)
                presentingViewController.present(authViewController, animated: true, completion: nil)
                self.authViewController = authViewController
            } else {
                self.signInFail(PianoIDError.presentingViewControllerFailed, completion: completion)
            }
        }
    }

    internal func getPresentingViewController() -> UIViewController? {
        if let viewController = presentingViewController {
            return viewController
        }

        if let window = PianoHelper.keyWindow, var controller = window.rootViewController {
            while let c = controller.presentedViewController, !c.isBeingDismissed {
                controller = c
            }
            
            return controller
        }

        return nil
    }

    internal func handleUrl(_ url: URL) -> Bool {
        guard url.absoluteString.lowercased().hasPrefix(urlSchemePrefix) else {
            return false
        }

        if let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false), let queryParams = urlComponents.queryItems {
            if let accessToken = queryParams.first(where: { $0.name == "access_token" })?.value,
               let refreshToken = queryParams.first(where: { $0.name == "refresh_token" })?.value {

                let token = PianoIDToken(accessToken: accessToken, refreshToken: refreshToken)
                signInSuccess(token, false)
                return true
            }

            if let code = queryParams.first(where: { $0.name == "code" })?.value {
                passwordlessSignIn(code: code)
                return true
            }
        }

        return false
    }

    internal func signInSuccess(_ token: PianoIDToken, _ isNewUser: Bool, completion: PianoIDSignInHandler? = nil) {
        _currentToken = token
        _ = PianoIDTokenStorage.shared.saveToken(token, aid: getAID())
        
        signInHandler {
            let result = PianoIDSignInResult(token, isNewUser)
            self.delegate?.signIn?(result: result, withError: nil)
            completion?(result, nil, false)
        }
    }

    internal func signInFail(_ error: Error!, completion: PianoIDSignInHandler? = nil) {
        signInHandler {
            self.delegate?.signIn?(result: nil, withError: error)
            completion?(nil, error, false)
        }
        
    }

    internal func signInCancel(completion: PianoIDSignInHandler? = nil) {
        signInHandler {
            self.delegate?.cancel?()
            completion?(nil, nil, true)
        }
    }

    internal func signOutSuccess() {
        if (delegate?.signOut != nil) {
            DispatchQueue.main.async {
                self.delegate!.signOut!(withError: nil)
            }
        }
    }

    internal func signOutFail() {
        if (delegate?.signOut != nil) {
            DispatchQueue.main.async {
                self.delegate!.signOut!(withError: PianoIDError.signOutFailed)
            }
        }
    }

    internal func logError(_ message: String) {
        print("PianoID: \(message)")
    }

    private func signInHandler(handler: @escaping () -> Void) {
        DispatchQueue.main.async {
            if let vc = self.authViewController, let presentingViewController = vc.presentingViewController {
                vc.canceled = true
                presentingViewController.dismiss(animated: true, completion: {
                    handler()
                })
            } else {
                handler()
            }
        }
    }
    
    private func check(error: Error?, response: URLResponse?, data: Data?) throws -> (HTTPURLResponse, Data) {
        if let error {
            throw error
        }
        
        guard let httpResponse = response as? HTTPURLResponse else {
            throw PianoHTTPError("HTTP response is null", statusCode: 0)
        }
        
        if httpResponse.statusCode != 200 {
            throw PianoHTTPError("Invalid HTTP status code", statusCode: httpResponse.statusCode)
        }
        
        guard let data else {
            throw PianoError("HTTP response data is null")
        }
        
        return (httpResponse, data)
    }
}

#if os(visionOS)
#else
@available(iOS 9.0, *)
extension PianoID: SFSafariViewControllerDelegate {

    public func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        signInCancel()
    }
}
#endif

@available(iOS 13.0, *)
extension PianoID: ASWebAuthenticationPresentationContextProviding {

    public func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        presentingViewController?.view.window
                ?? (delegate as? UIViewController)?.view.window
                ?? PianoHelper.keyWindow
                ?? ASPresentationAnchor()
    }
}

public extension PianoID {
    
    static func consents() -> String? {
        if !PianoConsents.shared.configuration.requireConsents {
            return nil
        }
        
        let consents = PianoConsents.shared.consents.map { purpose, consent in
            [
                "purpose": purpose.name,
                "mode": consent.mode.name,
                "products": consent.products.map { $0.name }
            ] as [String : Any]
        }
        return !consents.isEmpty ? JSONSerializationUtil.serializeObjectToJSONData(object: consents).base64EncodedString() : nil
    }
    
    static func addHeaders(request: inout URLRequest) {
        if !PianoID.shared.aid.isEmpty {
            request.setValue(PianoID.shared.aid, forHTTPHeaderField: "piano-app-id")
        }
        
        if let consents = PianoID.consents() {
            request.setValue(consents, forHTTPHeaderField: "Pn-Consents")
        }
    }
}
