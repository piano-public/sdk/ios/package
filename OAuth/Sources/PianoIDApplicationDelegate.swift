import UIKit

@objcMembers
public class PianoIDApplicationDelegate: NSObject {
    
    public static let shared = PianoIDApplicationDelegate()
    
    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        for p in PianoID.shared.socialProviders.values {
            p.application(application, didFinishLaunchingWithOptions: launchOptions)
        }
        appleApplication(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
        
    public func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        if PianoID.shared.handleUrl(url) {
            return true
        }
        
        for p in PianoID.shared.socialProviders.values {
            if p.application(application, handleOpen: url) {
                return true
            }
        }
        
        return false
    }

    public func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any?) -> Bool {
        if PianoID.shared.handleUrl(url) {
            return true
        }
        
        for p in PianoID.shared.socialProviders.values {
            if p.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation) {
                return true
            }
        }
        
        return false
    }

    public func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if PianoID.shared.handleUrl(url) {
            return true
        }
        
        for p in PianoID.shared.socialProviders.values {
            if p.application(application, open: url, options: options) {
                return true
            }
        }
        
        return false
    }
}
