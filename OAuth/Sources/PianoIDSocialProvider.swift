import UIKit

open class PianoIDSocialProvider {
    
    open var name: String { "" }
    
    public init() {}
    
    open func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) {}
    
    open func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        return false
    }
    
    open func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any?) -> Bool {
        return false
    }
    
    open func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return false
    }
    
    open func signIn(controller: UIViewController, completion: @escaping (Bool, String?, Error?) -> Void) {}
}
