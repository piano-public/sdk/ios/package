import UIKit
import WebKit

import PianoCommon

@objcMembers
class PianoIDOAuthViewController: UIViewController {
    
    private static let bundle = Bundle.load("PianoSDK_PianoOAuth", for: PianoIDOAuthViewController.self)

    let postMessageHandler = "postMessage"
    let loginSuccessMessageHandler = "loginSuccess"
    let registerSuccessMessageHandler = "registerSuccess"
    let socialLoginMessageHandler = "socialLogin"
    let customEventMessageHandler = "customEvent"
    let errorMessageHandler = "error"
    
    let stateReadyEvent = "stateReady"

    weak var mainWebView: WKWebView?
    
    var childWebView: [WKWebView] = []

    let webViewContentController = WKUserContentController()
    let estimatedProgressPropertyKeyPath = "estimatedProgress"
    let canGoBackPropertyKeyPath = "canGoBack"
    let canGoForwardPropertyKeyPath = "canGoForward"

    var url: URL?
    var code: String = ""
    var completion: PianoIDSignInHandler? = nil

    var navigationBar: UINavigationBar!
    var progressView: UIProgressView!

    var toolbar: UIToolbar!
    var backButton: UIBarButtonItem!
    var forwardButton: UIBarButtonItem!
    
    internal var canceled = false

    init(title: String?, url: URL?, completion: PianoIDSignInHandler? = nil) {
        super.init(nibName: nil, bundle: nil)

        self.url = url
        self.title = title
        self.completion = completion
    }

    required init?(coder: NSCoder) {
        super.init(nibName: nil, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        initViews()
    }

    override func viewWillDisappear(_ animated: Bool) {
        if isBeingDismissed {
            deinitWebViews()
        }
        
        if !canceled {
            PianoID.shared.signInFail(PianoIDError.presentingViewControllerFailed)
        }
    }

    fileprivate func initViews() {
        if #available(iOS 13.0, *) {
            isModalInPresentation = true
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }

        initNavigationItem()
        initNavigationBar()
        initToolbar()
        initWebView()
        setViewsConstraints()

        load()
    }

    fileprivate func initNavigationItem() {
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(onCancelButtonTouch(_:)))
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(onRefreshButtonTouch(_:)))
        navigationItem.setLeftBarButton(cancelButton, animated: false)
        navigationItem.setRightBarButton(refreshButton, animated: false)
    }

    fileprivate func initNavigationBar() {
        progressView = UIProgressView(progressViewStyle: .bar)
        progressView.isHidden = true

        navigationBar = UINavigationBar()
        navigationBar.sizeToFit()
        view.addSubview(navigationBar)

        navigationBar.setItems([navigationItem], animated: false)
        navigationBar.addSubview(progressView)
        navigationBar.isTranslucent = false
    }

    fileprivate func initToolbar() {
        toolbar = UIToolbar(frame: CGRect(origin: .zero, size: CGSize(width: view.bounds.width, height: 0.0)))
        toolbar.sizeToFit()
        view.addSubview(toolbar)

        let fixedSpace = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        fixedSpace.width = 60.0

        if let chevronLeftImage = UIImage(named: "piano_chevron", in: PianoIDOAuthViewController.bundle, compatibleWith: nil), let chevronLeftCGImage = chevronLeftImage.cgImage {
            backButton = UIBarButtonItem(image: chevronLeftImage, style: .plain, target: self, action: #selector(onBackButtonTouch(_:)))
            forwardButton = UIBarButtonItem(image: UIImage(cgImage: chevronLeftCGImage, scale: chevronLeftImage.scale, orientation: .upMirrored), style: .plain, target: self, action: #selector(onForwardButtonTouch(_:)))
        } else {
            backButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(onBackButtonTouch(_:)))
            forwardButton = UIBarButtonItem(title: "Forward", style: .plain, target: self, action: #selector(onForwardButtonTouch(_:)))
        }

        backButton.isEnabled = false
        forwardButton.isEnabled = false
        toolbar.setItems([backButton, fixedSpace, forwardButton], animated: false)
    }

    fileprivate func initWebView() {
        webViewContentController.add(self, name: postMessageHandler)
        webViewContentController.add(self, name: loginSuccessMessageHandler)
        webViewContentController.add(self, name: registerSuccessMessageHandler)
        webViewContentController.add(self, name: socialLoginMessageHandler)
        webViewContentController.add(self, name: customEventMessageHandler)
        webViewContentController.add(self, name: errorMessageHandler)

        let config = WKWebViewConfiguration()
        config.preferences.javaScriptEnabled = true
        config.preferences.javaScriptCanOpenWindowsAutomatically = false
        config.userContentController = webViewContentController
        config.websiteDataStore = WKWebsiteDataStore.nonPersistent()

        let webView = WKWebView(frame: .zero, configuration: config)
        webView.navigationDelegate = self
        webView.uiDelegate = self
        webView.addObserver(self, forKeyPath: estimatedProgressPropertyKeyPath, options: .new, context: nil)
        webView.addObserver(self, forKeyPath: canGoBackPropertyKeyPath, options: .new, context: nil)
        webView.addObserver(self, forKeyPath: canGoForwardPropertyKeyPath, options: .new, context: nil)

        if #available(macOS 13.3, iOS 16.4, tvOS 16.4, *) {
            webView.isInspectable = true
        }
        
        view.addSubview(webView)
        mainWebView = webView
    }

    fileprivate func deinitWebViews() {
        webViewContentController.removeScriptMessageHandler(forName: postMessageHandler)
        webViewContentController.removeScriptMessageHandler(forName: loginSuccessMessageHandler)
        webViewContentController.removeScriptMessageHandler(forName: registerSuccessMessageHandler)
        webViewContentController.removeScriptMessageHandler(forName: socialLoginMessageHandler)
        webViewContentController.removeScriptMessageHandler(forName: customEventMessageHandler)
        webViewContentController.removeScriptMessageHandler(forName: errorMessageHandler)

        if let webView = mainWebView {
            deinitWebView(webView)
            mainWebView = nil
        }

        childWebView.forEach { wv in
            deinitWebView(wv)
        }
        childWebView.removeAll()
    }

    fileprivate func deinitWebView(_ webView: WKWebView) {
        webView.stopLoading()
        webView.removeFromSuperview()
        webView.uiDelegate = nil
        webView.navigationDelegate = nil
        webView.removeObserver(self, forKeyPath: estimatedProgressPropertyKeyPath)
        webView.removeObserver(self, forKeyPath: canGoBackPropertyKeyPath)
        webView.removeObserver(self, forKeyPath: canGoForwardPropertyKeyPath)
    }

    fileprivate func setViewsConstraints() {
        if #available(iOS 9.0, *) {
            var topAnchor = view.topAnchor
            var bottomAnchor = view.bottomAnchor

            if #available(iOS 11.0, *) {
                topAnchor = view.safeAreaLayoutGuide.topAnchor
                bottomAnchor = view.safeAreaLayoutGuide.bottomAnchor
            }

            navigationBar.translatesAutoresizingMaskIntoConstraints = false
            navigationBar.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
            navigationBar.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
            navigationBar.topAnchor.constraint(equalTo: topAnchor).isActive = true

            progressView.translatesAutoresizingMaskIntoConstraints = false
            progressView.leftAnchor.constraint(equalTo: navigationBar.leftAnchor).isActive = true
            progressView.rightAnchor.constraint(equalTo: navigationBar.rightAnchor).isActive = true
            progressView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor).isActive = true

            toolbar.translatesAutoresizingMaskIntoConstraints = false
            toolbar.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
            toolbar.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
            toolbar.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true

            if let mainWebView = mainWebView {
                mainWebView.translatesAutoresizingMaskIntoConstraints = false
                mainWebView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
                mainWebView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
                mainWebView.topAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: 1.0).isActive = true
                mainWebView.bottomAnchor.constraint(equalTo: toolbar.topAnchor, constant: -1.0).isActive = true
            }
        } else {
            navigationBar.translatesAutoresizingMaskIntoConstraints = true
            navigationBar.autoresizingMask = [.flexibleWidth]
            navigationBar.frame = CGRect(
                    origin: .zero,
                    size: CGSize(width: view.bounds.width, height: navigationBar.bounds.height)
            )

            progressView.translatesAutoresizingMaskIntoConstraints = true
            progressView.autoresizingMask = [.flexibleTopMargin, .flexibleWidth]
            progressView.frame = CGRect(
                    origin: CGPoint(x: 0, y: navigationBar.bounds.height - progressView.bounds.height),
                    size: CGSize(width: view.bounds.width, height: progressView.bounds.height)
            )

            toolbar.translatesAutoresizingMaskIntoConstraints = true
            toolbar.autoresizingMask = [.flexibleTopMargin, .flexibleWidth]
            toolbar.frame = CGRect(x: 0, y: view.bounds.height - toolbar.bounds.height, width: view.bounds.width, height: toolbar.bounds.height)

            if let mainWebView = mainWebView {
                mainWebView.translatesAutoresizingMaskIntoConstraints = true
                mainWebView.autoresizingMask = [.flexibleTopMargin, .flexibleWidth, .flexibleHeight]
                mainWebView.frame = CGRect(
                        origin: CGPoint(x: 0, y: navigationBar.bounds.height),
                        size: CGSize(width: view.bounds.width, height: view.bounds.height - navigationBar.bounds.height - toolbar.bounds.height)
                )
            }
        }
    }

    func load() {
        if let url = self.url {
            let request = URLRequest(url: url)
            mainWebView?.load(request)
        }
    }

    func socialSignInCallback(aid: String, oauthProvider: String, socialToken: String, params: [String: Any] = [:]) {
        let body: [String: Any] = [
            "clientId": aid,
            "oauthProvider": oauthProvider,
            "socialToken": socialToken,
            "code": code,
            "params": params
        ]

        let jsonBody = JSONSerializationUtil.serializeObjectToJSONString(object: body)
        let script = "window.PianoIDMobileSDK.socialLoginCallback('\(jsonBody)');"
        mainWebView?.evaluateJavaScript(script, completionHandler: nil)
    }

    @objc
    func onCancelButtonTouch(_ sender: UIBarButtonItem) {
        canceled = true
        PianoID.shared.signInCancel(completion: completion)
    }

    @objc
    func onRefreshButtonTouch(_ sender: UIBarButtonItem) {
        let activeWebView = childWebView.last ?? mainWebView
        activeWebView?.reload()
    }

    @objc
    func onBackButtonTouch(_ sender: UIBarButtonItem) {
        if let activeWebView = childWebView.last {
            if activeWebView.canGoBack {
                activeWebView.goBack()
            } else {
                childWebView.removeLast()
                webViewDidClose(activeWebView)
            }
        } else if let activeWebView = mainWebView, activeWebView.canGoBack {
            activeWebView.goBack()
        }
    }

    @objc
    func onForwardButtonTouch(_ sender: UIBarButtonItem) {
        if let activeWebView = childWebView.last ?? mainWebView, activeWebView.canGoForward {
            activeWebView.goForward()
        }
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == estimatedProgressPropertyKeyPath, let webView = object as? WKWebView {
            progressView.setProgress(Float(webView.estimatedProgress), animated: true);
        }

        if (keyPath == canGoBackPropertyKeyPath || keyPath == canGoForwardPropertyKeyPath) {
            updateNavigationButtons()
        }
    }

    fileprivate func updateNavigationButtons() {
        backButton.isEnabled = !childWebView.isEmpty || (mainWebView?.canGoBack ?? false)
        forwardButton.isEnabled = (childWebView.last?.canGoForward ?? false) || ((childWebView.isEmpty) && mainWebView?.canGoForward ?? false)
    }
}

extension PianoIDOAuthViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        updateNavigationButtons()
        progressView.setProgress(0.0, animated: false)
        progressView.isHidden = false
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        progressView.setProgress(1.0, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.progressView.isHidden = true
        }

        let jsBridgeInitialScript =
            "window.PianoIDMobileSDK=window.PianoIDMobileSDK||{};" +
            "window.PianoIDMobileSDK.\(postMessageHandler)=" +
            "function(data){try{webkit.messageHandlers.\(postMessageHandler).postMessage(data)}catch(err){console.log(err)}};" +
            "window.PianoIDMobileSDK.\(loginSuccessMessageHandler)=" +
            "function(body){try{webkit.messageHandlers.\(loginSuccessMessageHandler).postMessage(body)}catch(err){console.log(err)}};" +
            "window.PianoIDMobileSDK.\(registerSuccessMessageHandler)=" +
            "function(body){try{webkit.messageHandlers.\(registerSuccessMessageHandler).postMessage(body)}catch(err){console.log(err)}};" +
            "window.PianoIDMobileSDK.\(socialLoginMessageHandler)=" +
            "function(body){try{webkit.messageHandlers.\(socialLoginMessageHandler).postMessage(body)}catch(err){console.log(err)}};" +
            "window.PianoIDMobileSDK.\(customEventMessageHandler)=" +
            "function(body){try{webkit.messageHandlers.\(customEventMessageHandler).postMessage(body)}catch(err){console.log(err)}};" +
            "window.PianoIDMobileSDK.\(errorMessageHandler)=" +
            "function(body){try{webkit.messageHandlers.\(errorMessageHandler).postMessage(body)}catch(err){console.log(err)}};"

        webView.evaluateJavaScript(jsBridgeInitialScript)
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        progressView.setProgress(1.0, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.progressView.isHidden = true
        }
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        progressView.setProgress(1.0, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.progressView.isHidden = true
        }
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        decisionHandler(.allow)
    }
}

extension PianoIDOAuthViewController: WKUIDelegate {

    func webViewDidClose(_ webView: WKWebView) {
        deinitWebView(webView)

        if webView == mainWebView {
            PianoID.shared.signInCancel(completion: completion)
            return
        }

        childWebView.removeAll(where: { $0 == webView })

        updateNavigationButtons()
        progressView.setProgress(0.0, animated: false)
        progressView.isHidden = false
    }

    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        let nextWebView = WKWebView(frame: .zero, configuration: configuration)
        nextWebView.navigationDelegate = self
        nextWebView.uiDelegate = self
        nextWebView.addObserver(self, forKeyPath: estimatedProgressPropertyKeyPath, options: .new, context: nil);
        nextWebView.addObserver(self, forKeyPath: canGoBackPropertyKeyPath, options: .new, context: nil)
        nextWebView.addObserver(self, forKeyPath: canGoForwardPropertyKeyPath, options: .new, context: nil)

        view.addSubview(nextWebView)
        view.bringSubviewToFront(nextWebView)
        view.bringSubviewToFront(navigationBar)
        view.bringSubviewToFront(toolbar)
        setWebViewConstraints(nextWebView)
        childWebView.append(nextWebView)

        updateNavigationButtons()
        return nextWebView
    }

    fileprivate func setWebViewConstraints(_ webView: WKWebView) {
        guard let mainWebView = mainWebView else {
            return
        }

        if #available(iOS 9.0, *) {
            webView.translatesAutoresizingMaskIntoConstraints = false
            webView.leftAnchor.constraint(equalTo: mainWebView.leftAnchor).isActive = true
            webView.rightAnchor.constraint(equalTo: mainWebView.rightAnchor).isActive = true
            webView.topAnchor.constraint(equalTo: mainWebView.topAnchor).isActive = true
            webView.bottomAnchor.constraint(equalTo: mainWebView.bottomAnchor).isActive = true
        } else {
            webView.translatesAutoresizingMaskIntoConstraints = true
            webView.frame = mainWebView.frame
            webView.autoresizingMask = [.flexibleTopMargin, .flexibleWidth, .flexibleHeight]
        }
    }
}

extension PianoIDOAuthViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        switch message.name {
        case postMessageHandler:
            postMessage(body: message.body as? String ?? "")
        case loginSuccessMessageHandler:
            loginSuccess(body: message.body as? String ?? "", isNewUser: false)
        case registerSuccessMessageHandler:
            loginSuccess(body: message.body as? String ?? "", isNewUser: true)
        case socialLoginMessageHandler:
            socialLogin(body: message.body as? String ?? "")
        case customEventMessageHandler:
            customEvent(body: message.body as? String ?? "")
        case errorMessageHandler:
            error(body: message.body as? String ?? "")
        default:
            logError("Unknown JS message handler: \"\(message.name)\"")
        }
    }
    
    func postMessage(body: String) {
        guard let json = body.parseJson() else {
            logError("Post message: incorrect input parameters")
            return
        }
        
        if let event = json["event"] as? String {
            switch event {
            case stateReadyEvent:
                if let consents = PianoHelper.consents() {
                    let script = "window.PianoIDMobileSDK.messageCallback(JSON.stringify({event: 'consentWithModes', params: JSON.stringify(\(consents))}))"
                    mainWebView?.evaluateJavaScript(script)
                }
            default:
                break
            }
        }
    }

    func loginSuccess(body: String, isNewUser: Bool) {
        guard let json = body.parseJson() else {
            logError("Login success: incorrect input parameters")
            return
        }

        if let accessToken = json["accessToken"] as? String {
            let refreshToken = json["refreshToken"] as? String ?? ""
            let idToken = PianoIDToken(accessToken: accessToken, refreshToken: refreshToken)
            PianoID.shared.signInSuccess(idToken, isNewUser, completion: completion)
        }
    }

    func socialLogin(body: String) {
        guard let json = body.parseJson(), let oauthProvider = json["oauthProvider"] as? String, let code = json["code"] as? String else {
            logError("Social login: incorrect input parameters")
            return
        }

        self.code = code
        
        let name = oauthProvider.lowercased()
        if name == "apple" {
            if #available(iOS 13.0, *) {
                PianoID.shared.appleSignIn()
            } else {
                logError("Apple Sign In not supported for current iOS version")
            }
            return
        }
        
        guard let p = PianoID.shared.socialProviders[name] else {
            logError("Unknown OAuth provider: \"\(oauthProvider)\"")
            return
        }
        
        p.signIn(controller: self) { cancel, token, error in
            if cancel {
                PianoID.shared.signInCancel(completion: self.completion)
                return
            }
            
            if let error {
                PianoID.shared.signInFail(error, completion: self.completion)
                return
            }
            
            guard let token else {
                PianoID.shared.signInFail(PianoIDError.signInFailed, completion: self.completion)
                return
            }
            
            self.socialSignInCallback(aid: PianoID.shared.aid, oauthProvider: name, socialToken: token)
        }
    }

    func customEvent(body: String) {
        guard let json = body.parseJson() else {
            logError("Custom event: incorrect input parameters")
            return
        }

        PianoID.shared.delegate?.customEvent?(event: json)
    }
    
    func error(body: String) {
        if let json = body.parseJson(), let message = json["message"] as? String {
            logError(message)
        } else {
            logError(body)
        }
        
        PianoID.shared.signInFail(PianoIDError.signInFailed)
    }

    func logError(_ message: String) {
        print("PianoID: \(message)")
    }
}
