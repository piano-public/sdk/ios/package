Pod::Spec.new do |s|
  s.name         = 'PianoCommon'
  s.version      = '2.8.6'
  s.swift_version = '5.10'
  s.summary      = 'Common iOS components by Piano.io'
  s.homepage     = 'https://gitlab.com/piano-public/sdk/ios/package'
  s.license      = { :type => 'Apache License, Version 2.0', :file => 'LICENSE' }
  s.author       = 'Piano Inc.'
  s.platform     = :ios, :tvos
  s.ios.deployment_target = '12.0'
  s.tvos.deployment_target = '12.0'
  s.source       = { :git => 'https://gitlab.com/piano-public/sdk/ios/package.git', :tag => "#{s.version}" }
  s.resource_bundle = {
      "PianoSDK_PianoCommon" => ["Common/Sources/Resources/*"]
  }
  s.source_files = 'Common/Sources/*.swift', 'Common/Sources/**/*.h'
  s.dependency 'PianoConsents', ">=1.0"
end
