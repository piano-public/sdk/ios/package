import Foundation

#if canImport(ObjectiveC)
@objcMembers
#endif
public class PianoEndpoint: NSObject {

    public static var production: PianoEndpoint {
        PianoEndpoint(api: "https://api.piano.io", composer: "https://c2.piano.io", id: "https://id.piano.io")
    }
    
    public static var productionEurope: PianoEndpoint {
        PianoEndpoint(api: "https://api-eu.piano.io", composer: "https://c2-eu.piano.io", id: "https://id-eu.piano.io")
    }

    public static var productionAustralia: PianoEndpoint {
        PianoEndpoint(api: "https://api-au.piano.io", composer: "https://c2-au.piano.io", id: "https://id-au.piano.io")
    }

    public static var productionAsiaPacific: PianoEndpoint {
        PianoEndpoint(api: "https://api-ap.piano.io", composer: "https://c2-ap.piano.io", id: "https://id-ap.piano.io")
    }

    public static var sandbox: PianoEndpoint {
        PianoEndpoint(api: "https://sandbox.piano.io", composer: "https://c2-sandbox.piano.io", id: "https://sandbox.piano.io")
    }

    public let api: String
    public let composer: String
    public let id: String

    public init(api: String, composer: String, id: String) {
        self.api = api
        self.composer = composer
        self.id = id
    }

    public convenience init(url: String) {
        self.init(api: url, composer: url, id: url)
    }
}
