import Foundation

public class PianoDictionary<TKey: Hashable, TValue> {
    
    private let queue: DispatchQueue
    
    private var dictionary: [TKey: TValue] = [:]
    
    public init(queue: DispatchQueue? = nil) {
        self.queue = queue ?? DispatchQueue(label: "PianoDictionary")
    }
    
    public subscript(index: TKey) -> TValue? {
        get {
            queue.sync {
                dictionary[index]
            }
        }
        set {
            queue.async {
                self.dictionary[index] = newValue
            }
        }
    }
    
    public func removeAll() {
        queue.async {
            self.dictionary.removeAll()
        }
    }
    
    public func toJSON() -> String {
        queue.sync {
            JSONSerializationUtil.serializeObjectToJSONString(object: dictionary)
        }
    }
}
