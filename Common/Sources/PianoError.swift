import Foundation

/// Piano common error
public class PianoError: NSObject, LocalizedError {
    /// Error message
    #if canImport(ObjectiveC)
    @objc
    #endif
    public let message: String

    /// Create instance of PianoError
    ///
    /// - Parameters:
    ///   - message: Error message
    #if canImport(ObjectiveC)
    @objc
    #endif
    public init(_ message: String) {
        self.message = message
    }

    /// Error description
    #if canImport(ObjectiveC)
    @objc
    #endif
    public var errorDescription: String? { message }
    
    /// Debug description
    #if canImport(ObjectiveC)
    @objc
    #endif
    public override var debugDescription: String { "Piano error:\n\t\(message)" }
}

/// Piano HTTP error
public class PianoHTTPError: PianoError {
    
    /// HTTP status code
    #if canImport(ObjectiveC)
    @objc
    #endif
    public let statusCode: Int
    
    /// Create instance of PianoHTTPError
    ///
    /// - Parameters:
    ///   - message: Error message
    ///   - statusCode: HTTP status code
    #if canImport(ObjectiveC)
    @objc
    #endif
    public init(_ message: String, statusCode: Int) {
        self.statusCode = statusCode
        super.init(message)
    }
}
