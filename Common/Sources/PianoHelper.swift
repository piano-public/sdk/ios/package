import Foundation

#if canImport(UIKit)
import UIKit
#endif

import PianoConsents

public struct PianoHelper {
    
    #if os(iOS) || os(visionOS)
    public static var keyWindow: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.connectedScenes
                .filter { $0.activationState == .foregroundActive }
                .first(where: { $0 is UIWindowScene })
                .flatMap({ $0 as? UIWindowScene })?.windows
                .first(where: \.isKeyWindow)
        } else {
            return UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        }
    }
    #endif
    
    public static func consents() -> String? {
        if !PianoConsents.shared.configuration.requireConsents {
            return nil
        }
        
        let consents = Dictionary(
            PianoConsents.shared.consents.map { purpose, consent in
                (
                    purpose.name,
                    [
                        "mode": consent.mode.name,
                        "products": consent.products.map { $0.name }
                    ] as [String : Any]
                )
            },
            uniquingKeysWith: { k, _ in k }
        )
        return !consents.isEmpty ? JSONSerializationUtil.serializeObjectToJSONString(object: consents) : nil
    }
}
