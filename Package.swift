// swift-tools-version:5.10

import PackageDescription

let package = Package(
    name: "PianoSDK",
    platforms: [
        .iOS(.v12),
        .tvOS(.v12),
        .visionOS(.v1),
        .macOS(.v10_13)
    ],
    products: [
        .library(
            name: "PianoCommon",
            targets: ["PianoCommon"]
        ),
        .library(
            name: "PianoOAuth",
            targets: ["PianoOAuth"]
        ),
        .library(
            name: "PianoComposer",
            targets: ["PianoComposer"]
        ),
        .library(
            name: "PianoTemplate",
            targets: ["PianoTemplate"]
        ),
        .library(
            name: "PianoTemplate.ID",
            targets: ["PianoTemplate.ID"]
        )
    ],
    dependencies: [
        .package(url: "https://gitlab.com/piano-public/sdk/ios/packages/consents", .upToNextMinor(from: "1.0.8")),
    ],
    targets: [
        /// Common
        .target(
            name: "PianoCommon",
            dependencies: [
                .product(name: "PianoConsents", package: "consents")
            ],
            path: "Common/Sources",
            resources: [
                .process("Resources")
            ]
        ),
        .testTarget(
            name: "PianoCommonTests",
            dependencies: ["PianoCommon"],
            path: "Common/Tests"
        ),
        /// OAuth
        .target(
            name: "PianoOAuth",
            dependencies: [
                "PianoCommon"
            ],
            path: "OAuth/Sources",
            resources: [
                .process("Resources")
            ]
        ),
        .testTarget(
            name: "PianoOAuthTests",
            dependencies: ["PianoOAuth"],
            path: "OAuth/Tests"
        ),
        /// Composer
        .target(
            name: "PianoComposer",
            dependencies: [
                "PianoCommon"
            ],
            path: "Composer/Sources",
            resources: [
                .copy("Resources/PrivacyInfo.xcprivacy")
            ]
        ),
        .testTarget(
            name: "PianoComposerTests",
            dependencies: ["PianoComposer"],
            path: "Composer/Tests"
        ),
        /// Template
        .target(
            name: "PianoTemplate",
            dependencies: [
                "PianoComposer"
            ],
            path: "Template/Core/Sources",
            resources: [
                .copy("Resources/PrivacyInfo.xcprivacy")
            ]
        ),
        .testTarget(
            name: "PianoTemplateTests",
            dependencies: ["PianoTemplate"],
            path: "Template/Core/Tests"
        ),
        /// Template.ID
        .target(
            name: "PianoTemplate.ID",
            dependencies: [
                "PianoOAuth",
                "PianoTemplate"
            ],
            path: "Template/ID/Sources",
            resources: [
                .copy("Resources/PrivacyInfo.xcprivacy")
            ]
        ),
        .testTarget(
            name: "PianoTemplateIDTests",
            dependencies: ["PianoTemplate.ID"],
            path: "Template/ID/Tests"
        )
    ]
)
